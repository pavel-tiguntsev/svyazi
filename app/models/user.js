var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');


var userSchema = mongoose.Schema({
    displayName: String,
    name: String,
    surname: String,
    regFrom: { type: Number, default: 0},
    vkId: String,
    fbId: String,
    email: String,
    password: String,
    registerStage: { type: Number, default: 0},
    created: { type: Date, default: Date.now },
    biday: [Number],
    geoloc: [Number],
    mainProf: String,
    education: [Object],
    work: [Object],
    skills: [Object],
    age: {type: Number, default: 0},
    city: String,
    country: String,
    svyazi: [Object],
    recBy: [Object],
    status: Number,
    contact: {
        phone : String,
        post : String,
        address: String,
        phone: String,
        viber: String,
        skype: String
    },
    profile: {
        dname: String,
        fname: String,
        sname: String,
        jobtitle: String,
        phone : String,
        bday: String,
        category: String,
        status: Boolean,
        comment: String
    },
    fb: Object,
    vk: Object
});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};


userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};


module.exports = mongoose.model('User', userSchema);