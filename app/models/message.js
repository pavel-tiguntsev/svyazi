var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');


var messageSchema = mongoose.Schema({
    displayNameFrom: String,
    displayNameTo: String,
    text: String,
    fromId: String,
    toId: String,
    created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Message', messageSchema);