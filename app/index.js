const express = require('express')
  , passport = require('./passport')
  , util = require('util')
  , config = require('../config.json')
  , mongoose = require('mongoose')
  , moment = require('moment');


var app = express();
var port = process.argv['port'] || 4000;
var server = require('http').Server(app);
var io = require('socket.io')(server);


io.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  socket.on('message', function (data) {
    socket.broadcast.emit(data.toId, data)
    console.log(data);
  });
});


app.set('views', __dirname + '/../views');
app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/../public'))
app.use(require('cookie-parser')());
app.use(require('body-parser')());
app.use(require('express-session')({ secret: 'RANDOM_LLOL999' }));
app.use(passport.initialize());
app.use(passport.session());

const User = require('./models/user.js');
const Message = require('./models/message.js')



app.get('/', function(req, res){
  if (req.isAuthenticated()) {
    res.redirect('/profile');
    return;
  } else {
    res.redirect('/login');
  }
});

app.get('/profile',  ensureAuthenticated, function(req, res){

  User.findOne({
    '_id': req.user._id
  }, function(err, doc) {
    if (doc.registerStage < 6) {
      res.redirect('/addinfo');
      return;
      } else {
        res.render('profile', { 
          user: doc,
          isHeaderBottom: true,
          title: doc.displayName,
          isLoginForm: false});
      }
    })

  })

  app.put('/profile', function(req, res) {
    console.log('PUT WORKS');
  })

  app.post('/editprofile', ensureAuthenticated, function(req, res) {
    var data = req.body;
    User.findOne({"_id": req.user._id}, (err, doc) => {
      doc.mainProf = data.mainProf;
      doc.save(() => {
        res.redirect('/profile')
      })
    })
  })

app.get('/login', function(req, res){
  res.render('index', { 
    user: req.user,
    isLoginForm: true,
    error: {
      is: true ? req.body.error : false,
      text: req.body.error
    }
  });
});

app.get('/users', ensureAuthenticated, function(req, res) {
  User.find({}, (err, docs) => {
    res.render('users', {
      user: req.user,
      myId: req.user._id,
      users: docs
    })
  })
})

app.post('/chat', ensureAuthenticated, function(req, res){

  var data = req.body;
  var sendtoId = data.toId;
  var newMsg = new Message();
  newMsg.displayNameFrom =  req.user.displayName;
  newMsg.displayNameTo = data.displayNameTo;
  newMsg.text = data.text
  newMsg.fromId = req.user._id
  newMsg.toId = data.toId

  newMsg.save(function(err, msg){

    // io.on('connection', function (socket) {
    //   socket.emit(sendtoId, { msg: msg });
    // });

  })
})


app.post('/login', passport.authenticate('local-login', {
  successRedirect : '/profile',
  failureRedirect : '/login'
}));



app.post('/signup', passport.authenticate('local-signup', {
  successRedirect : '/profile',
  failureRedirect : '/',
}));

app.get('/auth/facebook',
  passport.authenticate('facebook'));

app.get('/auth/facebook/callback',
  passport.authenticate('facebook', { failureRedirect: '/' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/profile');
  });

app.get('/auth/vk',
  passport.authenticate('vkontakte'),
  function(req, res){
  });

app.get('/auth/vk/callback', 
  passport.authenticate('vkontakte', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/profile');
  });

app.get('/addinfo', ensureAuthenticated, function(req, res){

    User.findOne({'_id': req.user._id}, (err,doc) => {
      var stage = doc.registerStage;
      if (stage > 6) {
        res.redirect("/profile")
      }
      res.render('addinfo', {
        user: doc,
        isHeaderBottom: false,
        stage: stage,
        isLoginForm: false,
      })  
    })
});

app.get('/inbox', ensureAuthenticated, function(req,res) {
  Message.find({
    toId: req.user._id
  }, function(err,resp){
    res.json(resp)
  })
})

app.get('/outbox', ensureAuthenticated, function(req,res) {
  Message.find({
    fromId: req.user._id
  }, function(err, resp) {
    res.json(resp)
  })
})


app.post('/addinfo', ensureAuthenticated, function(req, res){
  var data = req.body;
  var user = req.user;
  var stage = req.query.stage;
  User.findOne({
    '_id': user._id
  }, function(err, doc) {
    if(err) {
      console.log("ERROR");
      throw(err)
    }
    if (stage == 0) {
      doc.registerStage = 6;
      doc.biday = [ parseInt(data.bday) , parseInt(data.bmonth) , parseInt(data.byear)];
      doc.age = parseInt(moment().year()) - parseInt(data.byear);
      doc.save(() => {
        res.redirect('/addinfo');
      })
    }
    if (stage == 6) {
      doc.registerStage = 6;
      doc.save(() => {
        res.redirect('/profile');
      })
    }
  })
})

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});



mongoose.connect(config['MONGO_URL'], function(err) {
    if (err) throw err;
    console.log("Connected to URL: " + config['MONGO_URL']);
    
    server.listen(port);
});


function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}
