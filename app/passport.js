const User = require('./models/user.js');

var passport = require('passport'),
    VkStrategy = require('passport-vkontakte').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    LocalStrategy = require('passport-local').Strategy,
    config = require('../config.json');
    

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (obj, done) {
    done(null, obj);
});


passport.use(new VkStrategy({
        clientID: config['VK_APP_ID'],
        clientSecret: config['VK_APP_SECRET'],
        callbackURL: config['VK_URL_CALLBACK'],
        scope: ['email'],
        profileFields: ['email'],
    },
    function verify(accessToken, refreshToken, params, profile, done) {
        process.nextTick(function () {

            User.findOne({ vkId: profile.id }, function(err,user){
                console.log(user);
                if(err){
                    console.log("err: ", err);
                }
                if (user == null) {
                    console.log('debug', 1);
                    var newUser = new User();
                    newUser.vk = profile;
                    newUser.name = profile.name.familyName;
                    newUser.surname = profile.name.givenName;
                    newUser.displayName = profile.displayName;
                    newUser.regFrom = 1;
                    newUser.vkId = profile.id;
                    newUser.save(function(err, doc){
                        return done(null, doc);
                    })
                } else {
                    console.log('debug', 2);
                    return done(null, user);
                }
            })
        });
    }
));

passport.use(new FacebookStrategy({
        clientID: config['FB_APP_ID'],
        clientSecret: config['FB_APP_SECRET'],
        callbackURL: config['FB_URL_CALLBACK'],
    },
    function (accessToken, refreshToken, profile, cb) {

        User.findOne({ fbId: profile.id }, function(err,user){
            console.log(user);
            if(err){
                console.log("err: ", err);
            }
            if (user == null) {
                console.log('debug', 1);
                var newUser = new User();
                newUser.fb = profile;
                newUser.name = profile.displayName.slice(' ')[0];
                newUser.surname = profile.displayName.slice(' ')[1];
                newUser.displayName = profile.displayName;
                newUser.regFrom = 2;
                newUser.fbId = profile.id;
                newUser.save(function(err, doc){
                    return cb(null, doc);
                })
            } else {
                console.log('debug', 2);
                return cb(null, user);
            }
        });
    }
));

passport.use('local-signup', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'pass',
        passReqToCallback: true
    },
    function (req, email, password, done) {
        var data = req.body;

        User.findOne({
            'email': email
        }, function (err, user) {

            if (err)
                return done(err);
            if (user) {
                return done(null, false);
            } else {

                var newUser = new User();
                newUser.email = email;
                newUser.name = data.name;
                newUser.surname = data.surname;
                newUser.geoloc = [data.latitude, data.longitude],
                newUser.displayName = data.name + ' ' + data.surname;
                newUser.password = newUser.generateHash(password);

                newUser.save(function (err) {
                    if (err)
                        throw rr;
                    return done(null, newUser);
                });
            }

        });

    }));


passport.use('local-login', new LocalStrategy({

        usernameField: 'email',
        passwordField: 'pass',
        passReqToCallback: true
    },
    function (req, email, password, done) {

        User.findOne({
            'email': email
        }, function (err, user) {

            if (err)
                return done(err);
            if (!user)
                return done(null, false);
            if (!user.validPassword(password))
                return done(null, false);
            return done(null, user);
        });

    }));



module.exports = passport;