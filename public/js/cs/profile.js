var profile = new Vue({
    el: "#app",
    data: {
        myProfile: {
            mainProf: profileData.mainProf
        },
        toggleEditPanel: false
    },
    methods: {
        closepanel: function(event){
            this.toggleEditPanel = false
        },
        openpanel: function(event){
            this.toggleEditPanel = true
        }

    }
})